import {Component, OnInit} from '@angular/core';
import {User} from '../user.model';
import {UserService} from '../user.service';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {ServerService} from '../server.service';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit {
  user: User;
  id: number;

  constructor(private userService: UserService,
              private route: ActivatedRoute,
              private router: Router,
              private serverService: ServerService) {
  }

  ngOnInit() {
    this.route.params
      .subscribe(
        (params: Params) => {
          this.id = +params['id'];
          this.user = this.userService.getUser(this.id);
        }
      );
  }

  onEdit() {
    this.userService.startedEditing.next(this.id);
    this.router.navigate(['../', this.id, 'edit'], {relativeTo: this.route});
  }

  onDelete() {
    this.userService.deleteUser(this.id);
    this.serverService.onSave();
    this.router.navigate(['/users']);
  }
}
