import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {UserService} from './user.service';
import {User} from './user.model';

@Injectable()
export class ServerService {
  constructor(private http: Http,
              private userService: UserService) {

  }

  storeUsers() {
    return this.http.put('https://user-management-32b8c.firebaseio.com/users.json', this.userService.getUsers());

  }

  getUsers() {
    this.http.get('https://user-management-32b8c.firebaseio.com/users.json')
      .subscribe(
        (response: Response) => {
          console.log(response.json());
          const users: User[] = response.json();
          this.userService.setUsers(users);
        }
      );
  }

  onSave() {
    this.storeUsers()
      .subscribe(
        (response: Response) => {
          console.log(response);
        }
      );
  }

  onFetch() {
    this.getUsers();
  }
}
