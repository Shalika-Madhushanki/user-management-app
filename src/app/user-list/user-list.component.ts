import {Component, OnDestroy, OnInit} from '@angular/core';
import {User} from '../user.model';
import {UserService} from '../user.service';
import {Subscription} from 'rxjs/internal/Subscription';
import {Router} from '@angular/router';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit, OnDestroy {
  users: User[];
  subscription: Subscription;


  constructor(private userService: UserService,
              private router: Router,) {
  }


  ngOnInit() {
    this.subscription = this.userService.userChanged
      .subscribe(
        (users: User[]) => {
          this.users = users;
        }
      );
    this.users = this.userService.getUsers();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  onNewUser() {
    this.router.navigate(['users/new']);
  }
}
