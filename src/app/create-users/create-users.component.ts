import {Component, OnInit, ViewChild} from '@angular/core';
import {NgForm} from '@angular/forms';
import {User} from '../user.model';
import {UserService} from '../user.service';
import {ActivatedRoute, Router} from '@angular/router';
import {ServerService} from '../server.service';

@Component({
  selector: 'app-create-users',
  templateUrl: './create-users.component.html',
  styleUrls: ['./create-users.component.css']
})
export class CreateUsersComponent implements OnInit {
  today = new Date();
  todayDate = new Date(this.today.getFullYear(), this.today.getMonth(), this.today.getDate());
  genders = ['Male', 'Female'];
  user: User;
  @ViewChild('f') userInfoForm: NgForm;

  constructor(private userService: UserService,
              private router: Router,
              private route: ActivatedRoute,
              private serverService: ServerService) {
  }

  ngOnInit() {
  }

  onSubmit() {
    const value = this.userInfoForm.value;
    const newUser = new User(value.firstName, value.lastName, value.gender, value.address, value.phoneNo, value.dob, value.email, value.imgPath);
    this.userService.addUser(newUser);
    this.serverService.onSave();
    this.userInfoForm.reset();
  }

  onCancel() {
    this.router.navigate(['../'], {relativeTo: this.route});
  }


}
