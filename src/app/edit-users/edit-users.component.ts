import {Component, OnInit, } from '@angular/core';
import {UserService} from '../user.service';
import {User} from '../user.model';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {ServerService} from '../server.service';

@Component({
  selector: 'app-edit-users',
  templateUrl: './edit-users.component.html',
  styleUrls: ['./edit-users.component.css']
})
export class EditUsersComponent implements OnInit {
  editForm: FormGroup;
  editedItemIndex: number;
  editedItem: User;
  genders = ['Male', 'Female'];


  constructor(private userService: UserService,
              private route: ActivatedRoute,
              private router: Router,
              private serverService: ServerService) {
  }

  ngOnInit() {

    this.editForm = new FormGroup({
      'firstName': new FormControl(null, [Validators.pattern('[a-zA-Z ]*'), Validators.required]),
      'lastName': new FormControl(null, [Validators.pattern('[a-zA-Z ]*'), Validators.required]),
      'gender': new FormControl(null, Validators.required),
      'address': new FormControl(null, Validators.required),
      'dob': new FormControl(null, [Validators.required]),
      'phoneNo': new FormControl(null, [Validators.required, Validators.minLength(10), Validators.maxLength(10)]),
      'email': new FormControl(null, [Validators.email]),
      'imgPath': new FormControl(null)
    });


    this.route.params
      .subscribe(
        (params: Params) => {
          this.editedItemIndex = +params['id'];
          this.editedItem = this.userService.getUser(this.editedItemIndex);

          this.editForm.patchValue({
            'firstName': this.editedItem.firstName,
            'lastName': this.editedItem.lastName,
            'gender': this.editedItem.gender,
            'address': this.editedItem.address,
            'dob': this.editedItem.dateOfBirth,
            'phoneNo': this.editedItem.contactNo,
            'email': this.editedItem.email,
            'imgPath': this.editedItem.imagePath,
          });
        }
      );
  }


  onSubmit() {
    const value = this.editForm.value;
    const updatedUser = new User(value['firstName'], value['lastName'], value['gender'], value['address'], value['phoneNo'], value['dob'], value['email'], value['imgPath']);
    this.userService.updateUser(this.editedItemIndex, updatedUser);
    this.editForm.reset();
    this.serverService.onSave();
    this.onCancel();
  }


  onCancel() {
    this.router.navigate(['../'], {relativeTo: this.route});
  }
}
