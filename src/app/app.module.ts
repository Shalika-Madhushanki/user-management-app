import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import {AppComponent} from './app.component';
import {EditUsersComponent} from './edit-users/edit-users.component';
import {CreateUsersComponent} from './create-users/create-users.component';
import {UserDetailComponent} from './user-detail/user-detail.component';
import {UserService} from './user.service';
import {UserListComponent} from './user-list/user-list.component';
import {UserComponent} from './user/user.component';
import {AppRoutingModule} from './app.routing.module';
import {StartUserComponent} from './start-user/start-user.component';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
import {DropdownDirective} from './dropdown.directive';
import {ServerService} from './server.service';


@NgModule({
  declarations: [
    AppComponent,
    EditUsersComponent,
    CreateUsersComponent,
    UserDetailComponent,
    UserListComponent,
    UserComponent,
    StartUserComponent,
    DropdownDirective,
    PageNotFoundComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpModule,
    ReactiveFormsModule,
    NgbModule,
  ],
  providers: [UserService, ServerService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
