import {User} from './user.model';
import {Subject} from 'rxjs/internal/Subject';

export class UserService {
  startedEditing = new Subject<number>();
  userChanged = new Subject<User[]>();

  private userList: User[] = [
    new User(
      'Shalika',
      'Madhushanki',
      'Female',
      'No:07,SHOBA,Bemmulla',
      94717377514,
      '1995-11-27',
      'shali.madhushanki.15@cse.mrt.ac.lk',
      'https://cdn0.iconfinder.com/data/icons/user-pictures/100/female-512.png'
    ),
    new User(
      'Dinika',
      'Senarath',
      'Female',
      'No:71/A,Veragoda,Pallewela',
      94712026357,
      '1995-07-21',
      'Dinika.15@cse.mrt.ac.lk',
      'https://images.onlinelabels.com/images/clip-art/dagobert83/dagobert83_female_user_icon.png'
    ),
    new User(
      'Hemal',
      'Suuriyabandara',
      'Male',
      'No:5/A,kubaldeniya,Pasyala',
      +'94714748224',
      '1992-02-21',
      'hemal1992.er@yahoo.lk',
      'https://thumbs.dreamstime.com/b/faceless-businessman-avatar-man-suit-blue-tie-human-profile-userpic-face-features-web-picture-gentlemen-85824471.jpg'
    ),
    new User(
      'Anuradha',
      'Karunarathna',
      'Female',
      'No:52,Kurulu Vimana,Bemmulla',
      94714748224,
      '1995-09-28',
      'Anuradha.15@cse.mrt.ac.lk',
      'https://lemonlearning.fr/wp-content/uploads/2017/05/businesswoman-300x300.png'
    ),
    new User(
      'Dilsha',
      'Karunathilaka',
      'Female',
      'No:4/5,Bandaranayaka Mawatha,Wadurawa',
      94778255854,
      '1995-09-28',
      'disha.nirmani@gmail.com',
      'https://image.flaticon.com/icons/png/512/190/190684.png'
    ),
    new User(
      'Chamodh',
      'Amarasinghe',
      'Male',
      'No:55,Samagi Mawatha, Palatotawatta,Kalutara South',
      +'94782541554',
      '2006-08-4',
      'chamodh.kaweesha06@gmail.com',
      'http://images.clipartpanda.com/user-clipart-xigojzxKT.png'
    )
  ];

  getUsers() {
    return this.userList.slice();
  }

  getUser(id: number) {
    return this.userList[id];
  }

  addUser(user: User) {
    this.userList.push(user);
    this.userChanged.next(this.userList.slice());
  }

  updateUser(index: number, newUser: User) {
    this.userList[index] = newUser;
    this.userChanged.next(this.userList.slice());
  }

  deleteUser(index: number) {
    this.userList.splice(index, 1);
    this.userChanged.next(this.userList.slice());
  }

  setUsers(users: User[]) {
    this.userList = users;
    this.userChanged.next(this.userList.slice());
  }
}
