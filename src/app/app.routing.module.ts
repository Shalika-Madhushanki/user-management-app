import {UserDetailComponent} from './user-detail/user-detail.component';
import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {CreateUsersComponent} from './create-users/create-users.component';
import {EditUsersComponent} from './edit-users/edit-users.component';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
import {StartUserComponent} from './start-user/start-user.component';

/*const appRoutes:Routes=[
  {path: '', redirectTo: '/users', pathMatch: 'full'},
  { path: 'users/new', component: CreateUsersComponent},
  {path: 'users', component: StartUserComponent,
    children: [
      { path: '', component: UserListComponent },
      {path: ':id', component: UserDetailComponent},
      { path: 'users/:id/edit', component: EditUsersComponent},
    ],
  },
  { path: 'not-found', component: PageNotFoundComponent},
  { path: '**', redirectTo: '/not-found'},
];*/


const appRoutes: Routes = [
  {path: '', redirectTo: '/users', pathMatch: 'full'},
  {path: 'users', component: StartUserComponent},
  {path: 'users/new', component: CreateUsersComponent},
  {path: 'users/:id', component: UserDetailComponent},
  {path: 'users/:id/edit', component: EditUsersComponent},
  {path: '**', component: PageNotFoundComponent}
  // {path: '**', redirectTo: '/not-found'},
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}

