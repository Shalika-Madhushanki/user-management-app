export class User {
  firstName: string;
  lastName: string;
  gender: string;
  address: string;
  contactNo: number;
  dateOfBirth: string;
  email: string;
  imagePath: string;

  constructor(fName: string, lName: string, gender: string, address: string, contactNo: number, dob: string, email: string, imagePath: string) {
    this.firstName = fName;
    this.lastName = lName;
    this.gender = gender;
    this.address = address;
    this.contactNo = contactNo;
    this.dateOfBirth = dob;
    this.email = email;
    this.imagePath = imagePath;
  }

}
